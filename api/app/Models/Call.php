<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 20.04.2019
 * Time: 14:24
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Call extends Model
{
	protected $fillable = [
		'id',
		'number',
		'callstart',
		'disposition',
	];

	public static function search(Request $request)
	{
		$perpage = $request->get('pagination')['perpage'] ?? 10;
		$page = $request->get('pagination')['page'] ?? 1;
		$field = in_array($request->get('sort')['field'], (new self())->fillable) ? $request->get('sort')['field'] : 'id';
		$sort = $request->get('sort')['sort'] ?? 'desc';
		$models =  self
			::when($request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->select('calls.*', 'c.name', 'c.id as c_id')
			->leftJoin('contacts as c', function($join) {
				$join->on('calls.number', 'LIKE', DB::raw('CONCAT("%", substring(c.number, -9), "%")'));
			})
			->when(!$request->get('orderBy'), function ($models) use ($request, $field, $sort) {
				return $models
					->orderBy($field, $sort);
			});

//		if (isset($request->get('query')['generalSearch'])) {
//			$globalSearch = $request->get('query')['generalSearch'];
//			$models
//				->leftJoin('contact_type_list as ctl', 'ctl.id', '=', 'contacts.client_type')
//				->leftJoin('positions as p', 'p.id', '=', 'contacts.position_id')
//				->orWhere('p.name', 'LIKE', "%{$globalSearch}%")
//				->orWhere('ctl.name', 'LIKE', "%{$globalSearch}%")
//				->orWhere('contacts.id', 'LIKE', "%{$globalSearch}%")
//				->orWhere('contacts.name', 'LIKE', "%{$globalSearch}%")
//				->orWhere('contacts.phone', 'LIKE', "%{$globalSearch}%")
//				->orWhere('contacts.email', 'LIKE', "%{$globalSearch}%")
//				->orWhere('contacts.town', 'LIKE', "%{$globalSearch}%")
//				->orWhere('contacts.address', 'LIKE', "%{$globalSearch}%");
//		}

		$count = $models->get()->count();
		$models = $models
			->when($request->get('page') >= 0 && !$request->get('all'), function ($users) use ($request, $perpage, $page){
				return $users->skip(($page - 1) * $perpage)->take($perpage);
			})
			->get();
		return response()->json([
			'models' => $models,
			'meta' => [
				'page' =>  $page,
				'pages' => ceil($count/10),
				'perpage' => $perpage,
				'total' => $count,
			]
		]);
	}

}
