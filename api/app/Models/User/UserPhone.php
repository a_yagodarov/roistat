<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 08.04.2019
 * Time: 0:05
 */

namespace App\Models\User;


use Illuminate\Database\Eloquent\Model;

class UserPhone extends Model
{
	public $table='user_phones';


	protected $fillable = [
		'id', 'number', 'user_id'
	];

}
