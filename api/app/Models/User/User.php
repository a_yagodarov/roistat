<?php

namespace App\Models\User;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
	use HasRoles;
    use Notifiable;
	protected $guard_name = 'api';
	public $pw;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $fillable = [
        'id', 'username', 'email', 'password', 'blocked', 'first_name', 'surname', 'patronymic',
        'terminal'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	public static function search(Request $request)
	{

	    $perpage = $request->get('pagination')['perpage'] ?? 10;
	    $page = $request->get('pagination')['page'] ?? 1;
	    $field = $request->get('sort')['field'] ?? 'id';
        $sort = $request->get('sort')['sort'] ?? 'desc';
        $globalSearch = $request->get('query')['generalSearch'];
        $users =  User::
			select('users.*', DB::raw("CONCAT_WS(' ', users.first_name, users.surname, users.patronymic) as full_name"), 'roles.name as role')
			->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
			->leftJoin('roles', 'model_has_roles.role_id', '=', 'roles.id')
			->with('phones')
			->when($request->get('username'), function($users) use ($request){
				return $users->where('users.username', 'LIKE', "%{$request->get('username')}%");
			})
			->when($globalSearch, function($users) use ($globalSearch){
				return $users
                    ->orWhere('roles.name', 'LIKE', "%{$globalSearch}%")
                    ->orWhere('roles.title', 'LIKE', "%{$globalSearch}%")
                    ->orWhere('users.id', '=', $globalSearch)
                    ->orWhere('users.username', 'LIKE', "%{$globalSearch}%")
                    ->orWhere('users.email', 'LIKE', "%{$globalSearch}%")
                    ->orWhere('users.first_name', 'LIKE', "%{$globalSearch}%")
                    ->orWhere('users.surname', 'LIKE', "%{$globalSearch}%")
                    ->orWhere('users.patronymic', 'LIKE', "%{$globalSearch}%");
			})
			->when($request->get('full_name'), function($users) use ($request){
				$array = explode(" ", $request->get('full_name'));
				$array = "%".implode("%", $array)."%";
				return $users->whereRaw("CONCAT_WS(' ', users.first_name, users.surname, users.patronymic) LIKE '{$array}'");
			})
			->when($request->get('email'), function($users) use ($request){
				return $users->where('users.email', 'LIKE', "%{$request->get('email')}%");
			})
			->when($request->get('orderBy'), function ($users) use ($request) {
				return $users
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when(!$request->get('orderBy'), function ($users) use ($request, $field, $sort) {
				return $users
					->orderBy($field, $sort);
			})
			->when($request->get('blocked') > -1, function ($users) use ($request){
				return $users->where('users.blocked', '=', $request->get('blocked'));
			})
			->when($request->get('role'), function ($users) use ($request){
				return $users->where('roles.name', '=', $request->get('role'));
			});

		$count = $users->get()->count();
		$users = $users

			->when($request->get('page') >= 0 && !$request->get('all'), function ($users) use ($request, $perpage, $page){
				return $users->skip(($page - 1) * $perpage)->take($perpage);
			})
			->get();
		return response()->json([
			'models' => $users,
            'meta' => [
                'page' =>  $page,
                'pages' => ceil($count/10),
                'perpage' => $perpage,
                'total' => $count,
            ]
		]);
	}

	public function rules(Request $request)
	{
		if ($request->isMethod('post')) {
			return [
				'username' => 'required|min:4|max:191|unique:users,username',
				'password' => 'nullable|min:4|max:191',
				'first_name' => 'required',
				'surname' => 'required',
				'patronymic' => 'required',
				'terminal' => 'required_if:role,manager',
				'email' => 'required|email|unique:users,email',
				'role' => 'required'
			];
		}
		elseif ($request->isMethod('put')) {
			return [
				'username' => 'required|min:4|max:191|unique:users,username,'.$request->get('id'),
				'password' => 'sometimes|nullable|min:4|max:191',
				'first_name' => 'required',
				'surname' => 'required',
				'patronymic' => 'required',
				'terminal' => 'required_if:role,manager',
				'email' => 'required|email|unique:users,email,'.$request->get('id'),
				// 'email' => 'required|email|unique:users,email,'.$request->get('id'),
				'blocked' => [ 'sometimes', function($attribute, $value, $fail) {
		            if (Auth::user()->id == $this->id && $this->blocked == 1) {
		                return $fail("Нельзя заблокировать себя");
		            }
		        },],
				'role' => [ 'sometimes', function($attribute, $value, $fail) {
		            if (Auth::user()->id == $this->id && $value != $this->getRoleNames()[0]) {
		                return $fail("Нельзя сменить свою роль");
		            }
		        },]
			];
		}
		else {
			abort(403);
		}
	}

	public function store(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill(Input::all());
			if ($this->password == ''){
                $this->pw = str_random(6);
	            $this->password = bcrypt($this->pw);
            }
            else
            {
                $this->pw = $request->password;
	            $this->password = bcrypt($request->password);
            }
			if ($result = $this->save())
			{
				$profile = Profile::create(['id' => $this->id]);
				$profile->save();
				if ($this->setRole($request) && $this->storePhones($request)) {
					return response()->json($result, 200);
					@$this->sendRegistrationEmail($this);
				} else {
					$this->delete();
				}
			}
			else
				return response()->json($result, 403);
		}
	}

	public function storeUpdate(Request $request)
    {
    	$this->fill(Input::all());
        $validate = Validator::make($request->all(), $this->rules($request), $this->messages());
        if (!$validate->fails() && $user = \App\Models\User\User::find($request->get('id')))
        {
        	$this::find($request->get('id'));
	        $this->pw = $request->password;
	        if (!$request->password)
	        {
		        $this->password = $this->getOriginal('password');
	        }
	        else
	        {
		        $this->password = bcrypt($request->password);
	        }
	        if ($this->pw || ($this->getOriginal('username') != $this->username))
	        	@$this->sendUpdateUserEmail($this);
            if ($result = $this->save())
            {
            	$this->setRole($request);
				$this->storePhones($request);
                return response()->json($result, 200);
            }
            else
            {
                return response()->json($result, 403);
            }
        }
        else
        {
            return response()->json($validate->errors(), 403);
        }
    }

    public function deletePhones() {
		if ($this->id) {
			UserPhone::where([
				['user_id', '=', $this->id]
			])->delete();
		}
	}

    public function storePhones($request)
	{
		$this->deletePhones();
		if ($phones = $request->get('phones')) {
			foreach ($phones as $phone) {
				if ($phone['number'] != '') {
					$userPhone = new UserPhone();
					$userPhone->number = $phone['number'];
					$userPhone->user_id = $this->id;
					$userPhone->save();
				}
			}
		}
		return true;
	}
    public function setRole($request)
    {
    	if ($request->role)
    	{
    		if (count($this->getRoleNames()) > 0)
    			$this->removeRole($this->getRoleNames()[0]);
	        return $this->assignRole($request->role);
    	}
    }

	public function messages() {
		return [
			'required' => 'Заполните это поле',
			'required_if' => 'Заполните это поле',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
		];
	}

    public function sendUpdateUserEmail($user)
    {
	    if ($this->pw)
	    {
		    $messsage = 'Логин: '.$user->username.'. Пароль: '.$this->pw.'.'.' Для сайта '.$this->getUrl();
	    }
	    else
	    {
		    $messsage = 'Логин: '.$user->username.' Для сайта '.$this->getUrl();
	    }
        return Mail::raw($messsage, function ($message) use ($user){
            $message->to($user->email);
            $message->subject('Данные для входа на сайт были обновлены');
        });
    }

    public function sendRegistrationEmail($user)
    {
        $messsage = 'Логин: '.$user->username.'. Пароль: '.$this->pw.'.'.' Для сайта '.$this->getUrl();
        return Mail::raw($messsage, function ($message) use ($user){
	            $message->to($user->email);
            $message->subject('Данные для входа на сайт');
        });
    }
    public function getUrl()
    {
    	$url = App::make('url')->to('/');
    	return parse_url($url, PHP_URL_SCHEME).'://'.str_replace('api', '', parse_url($url, PHP_URL_HOST));
    }

	public function phones()
	{
		return $this->hasMany(UserPhone::class, 'user_id', 'id');
	}
}
