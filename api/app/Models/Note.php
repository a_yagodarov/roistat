<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 16.04.2019
 * Time: 14:05
 */

namespace App\Models;


use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
	protected $fillable = [
		'id',
		'user_id',
		'comment',
		'contact_id',
		'note_type',
	];

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id', 'id');
//		return $this->hasOne(User::class, 'user_id', 'id');
	}
}
