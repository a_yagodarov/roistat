<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 20.04.2019
 * Time: 10:34
 */
//include_once "../../vendor/zadarma/user-api-v1/lib/Client.php";
namespace App\Models;


use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Mockery\Exception;
use Zadarma_API\Client;

class Zadarma
{
//	private static $key = 'bbb7f7a3f5a7f2461e07';
	public $key;
//	private static $secret = 'ca606dc8ccc53dc27e2b';
	public $secret;


	public function __construct()
	{
		$this->key = config('api.zadarma.key');
		$this->secret = config('api.zadarma.secret');
	}

//	public function getSipIds() {
//		$zd = new Client($this->key, $this->secret);
////		$params = [
////			'status ' => 'on',
////			'id' => '534200'
////		];
//		$answer = $zd->call("/v1/pbx/internal/", [], 'get');
//
//		return $result = json_decode($answer);
//	}

//	public function setListenCalls() {
//		$zd = new Client($this->key, $this->secret);
//		$params = [
//			'status ' => 'on',
//			'id' => '157512'
//		];
//		$answer = $zd->call("/v1/pbx/internal/recording/", $params, 'put');
//
//		return $result = json_decode($answer);
//	}

	public function getAudio($id) {
		$zd = new Client($this->key, $this->secret);
		$params = [
			'call_id' => $id
		];
		$answer = $zd->call("/v1/pbx/record/request/", $params, 'get');

		return $result = json_decode($answer);
	}

	public function fillCalls() {
		$zd = new Client($this->key, $this->secret);

//		$params = [
//			'start' => Carbon::now()->subSeconds(5)->toDateTimeString(),
//			'end' => Carbon::now()->toDateTimeString(),
//		];
		$params = [
			'start' => Carbon::now()->startofMonth(),
			'end' => Carbon::now()->lastOfMonth(),
		];

		$answer = $zd->call('/v1/statistics/', $params, 'get');

		$result = json_decode($answer);
		$return = [
			"calls" => 0,
			"contacts" => 0,
		];
		if ($result->status == 'success') {
			foreach ($result->stats as $call) {
				if ($user = User::whereHas('phones', function ($query) use ($call) {
					$phone = substr($call->to, -9);
					$query->where('user_phones.number', 'LIKE', "%{$phone}%");
				})->first()) {
					$this->saveCall($call, $call->from) ? ++$return['calls'] : null ;
					$this->createContact($call->from, $user) ? ++$return['contacts'] : null ;
				} elseif ($user = User::whereHas('phones', function ($query) use ($call) {
					$phone = substr($call->from, -9);
					$query->where('user_phones.number', 'LIKE', "%{$phone}%");
				})->first()) {
					$this->saveCall($call, $call->to) ? ++$return['calls'] : null ;
					$this->createContact($call->to, $user) ? ++$return['contacts'] : null ;
				}
			}
		} else {
			$result->message;
		}
		return $return;
	}

	public function saveCall($call, $number) {
		$callObject = new Call();
		$callObject['id'] = $call->id;
		$callObject['disposition'] = $call->disposition;
		$callObject['number'] = $number;
		$callObject['callstart'] = $call->callstart;
		$callObject['created_at'] = $call->callstart;
		return $callObject->save();
	}

	public function createContact($number, $user) {
		$contact = Contact::where('number', 'LIKE', "{$number}")->first();
		if (!$contact) {
			$contact = new Contact();
			$contact->number = $number;
			$contact->user_id = $user->id;
			return $contact->save();
		} else {
			return false;
		}
	}
}
