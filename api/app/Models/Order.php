<?php

namespace App\Models;

use App\Models\User\User;
use App\OrderJob;
use App\OrderPayment;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    public function rules($request)
    {
        if ($request->isMethod('post')) {
            return [
                'execution_date' => 'required'
            ];
        }
        elseif ($request->isMethod('put')) {
            return [
                'execution_date' => 'required'
            ];
        }
    }
    protected $fillable = [
        'id',
        'customer',
        'address',
        'phone_number',
        'comment',
        'payment_type',
        'cost',
        'execution_date',
    ];

    public static function search(Request $request)
    {
        $perpage = $request->get('pagination')['perpage'] ?? 10;
        $page = $request->get('pagination')['page'] ?? 1;
        $field = in_array($request->get('sort')['field'], (new Order())->fillable) ? $request->get('sort')['field'] : 'id';
        $sort = $request->get('sort')['sort'] ?? 'desc';
        $globalSearch = $request->get('query')['generalSearch'];
        $models =  Order::when($request->get('orderBy'), function ($models) use ($request) {
                return $models
                    ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
            })
            ->when($globalSearch, function($models) use ($globalSearch){
                return $models
                    ->orWhere('orders.customer', 'LIKE', "%{$globalSearch}%")
                    ->orWhere('orders.phone_number', 'LIKE', "%{$globalSearch}%")
                    ->orWhere('orders.cost', 'LIKE', "%{$globalSearch}%")
                    ->orWhere('orders.address', 'LIKE', "%{$globalSearch}%");
            })
            ->when(!$request->get('orderBy'), function ($users) use ($request, $field, $sort) {
                return $users
                    ->orderBy($field, $sort);
            })
            ->with('jobs')
            ->with('user')
            ->with('payments')
            ->with('jobs.performer');

        $count = $models->get()->count();
        $models = $models

            ->when($request->get('page') >= 0 && !$request->get('all'), function ($models) use ($request){
                return $models->skip($request->get('page') * 10)->take(10);
            })
            ->get();
        return response()->json([
            'models' => $models,
            'count' => $count
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
        if ($validator->fails()) {
            return response()->json($validator->messages(), 403);
        }
        else
        {
            $this->fill($request->all());
            $this->user_id = Auth::user()->id;
            if ($result = $this->save())
            {
                $this->saveJobs($request);
                $this->savePayments($request);
                return response()->json($result, 200);
            }
            else
                return response()->json($result, 403);
        }
    }

    public function storeUpdate(Request $request)
    {
        $validate = Validator::make($request->all(), $this->rules($request), $this->messages());
        if (!$validate->fails())
        {
            $this->fill($request->all());
            if ($result = $this->save())
            {
                $this->saveJobs($request);
                $this->savePayments($request);
                return response()->json($result, 200);
            }
            else
            {
                return response()->json($result, 403);
            }
        }
        else
        {
            return response()->json($validate->errors(), 403);
        }
    }

    public function messages() {
        return [
            'required' => 'Заполните это поле',
            'min' => 'Не менее :min символа(-ов)',
            'max' => 'Не более :max символа(-ов)',
            'unique' => 'Уже используется',
            'email' => 'Введите правильный формат email',
        ];
    }

    public function saveJobs(Request $request)
    {
        $jobsList = $request->get('jobs');
        foreach ($jobsList as $key => $value)
        {
            if ($orderJob = OrderJob::where([
                ['name' , '=', $key],
                ['order_id', '=', $this->id]
            ])->first())
            {
                $orderJob->fill($value);
//                $value['date'] ? $orderJob->date = Carbon::parse($value['date'])->toDateTimeString() : $orderJob->date = null;
                $orderJob->save();
            }
            else {
                $orderJob = new OrderJob();
                $orderJob->fill($value);
                $orderJob->name = $key;
                $orderJob->order_id = $this->id;
                $value['date'] ? $orderJob->date = Carbon::parse($value['date'])->toDateTimeString() : $orderJob->date = null;
                $orderJob->save();
            }
        }
    }

    public function savePayments(Request $request)
    {
        if (Auth::user()->hasRole('admin')){
            $payments = $request->get('payments');
            OrderPayment::where([
                ['order_id', '=', $this->id]
            ])->delete();
            foreach($payments as $payment)
            {
                if ($payment['payment'] && $payment['status'])
                {
                    $orderPayment = new OrderPayment();
                    $orderPayment->fill($payment);
                    $orderPayment->order_id = $this->id;
                    $orderPayment->save();
                }
            }
        }
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function jobs()
    {
        return $this->hasMany(OrderJob::class, 'order_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany(OrderPayment::class, 'order_id', 'id');
    }

    public function setExecutionDateAttribute($value)
    {
        $this->attributes['execution_date'] = Carbon::parse($value)->toDateTimeString();
    }
}
