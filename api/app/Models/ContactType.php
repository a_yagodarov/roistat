<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 15.04.2019
 * Time: 12:31
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ContactType extends Model
{
	protected $table = 'contact_type_list';
}
