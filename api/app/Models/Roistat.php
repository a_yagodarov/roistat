<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 24.04.2019
 * Time: 13:43
 */

namespace App\Models;


use GuzzleHttp\Client;

class Roistat
{
	private $key;
	private $project;

	public function __construct()
	{
		$this->key = config('api.roistat.key');
		$this->project = config('api.roistat.project');
	}

	public function fillContacts() {
		$contacts = Contact::all();
		$return = [
			'contacts' => 0
		];
		foreach ($contacts as $contact) {
			$contact->fillContactInfo() ?? ++$return['contacts'];
		}
		return $return;
	}

}
