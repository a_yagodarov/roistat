<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIsBlocked
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

    	if ($user = Auth::user()->blocked == 1) {

			return response()->json([
				'blocked' => true
			], 403);

		} else {

			return $next($request);

		}
    }
}
