<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Call as BaseModel;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class CallController extends Controller
{
	public function index(Request $request)
	{
		return BaseModel::search($request);
	}

//	public function get($id)
//	{
//		$user = BaseModel::find($id);
//		$user->notes;
//		foreach ($user->notes as $note) {
//			$note->user;
//		}
//		return response()->json($user);
//	}
//
//	public function store(Request $request)
//	{
//		return (new BaseModel())->store($request);
//	}
//
//	public function update(Request $request)
//	{
//		$user = BaseModel::find($request->get('id'));
//		return ($user->store($request));
//	}
//
//	public function delete($id)
//	{
//		return response()->json(BaseModel::find($id)->delete());
//	}
}
