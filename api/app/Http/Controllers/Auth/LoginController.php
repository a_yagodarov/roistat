<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\ContactType;
use App\Models\Position;
use App\Models\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller
{
    public function verify(Request $request)
    {
        if ($request->header('Authorization') && $token = JWTAuth::parseToken())
        {
            return response()->json(true, 200);
        } else {
            return response()->json(false, 200);
        }
    }

    public function getUser()
	{
		$user = User::where('id', Auth::user()->id)->first();
		$data = [];
		$data['user'] = $user;
		$data['options']['contact_types_list'] = ContactType::all();
		$data['options']['positions'] = Position::all();
		return response()->json($data, 200);
	}

	public function authenticate(Request $request)
	{
		// grab credentials from the request
		$credentials = $request->only('username', 'password');
		try {
			$user = User::where('username', $request->get('username'))->first();
			if ($user && $user->blocked)
			{
				return response()->json('Аккаунт заблокирован', 401);
			}
			if (! $token = JWTAuth::attempt($credentials)) {
				return response()->json('Неверные данные', 401);
			}
		} catch (JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return response()->json('Ошибка сервера', 500);
		}

		// all good so return the token
		$role = $user->getRoleNames()[0];
		$result = $user;
		$user['permissions_list'] = $permissions = implode(",", $user->getAllPermissions()->pluck('name')->toArray());
		$result['accessToken'] = $token;
		$result['role'] = $role;
		$result['roles'] = [0 => 1];
		$result['fullname'] = $role;
		$result['companyName'] = $role;
		return response()->json($result);
	}
}
