<?php

namespace App\Http\Controllers;

use App\Models\Order as BaseModel;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        return BaseModel::search($request);
    }

    public function get($id)
    {
        $model = BaseModel::find($id);
        $model->jobs;
        $model->payments;
        return response()->json($model);
    }

    public function store(Request $request)
    {
        return (new BaseModel())->store($request);
    }

    public function update(Request $request)
    {
        $model = BaseModel::find($request->get('id'));
        return ($model->storeUpdate($request));
    }

    public function delete($id)
    {
        return response()->json(BaseModel::find($id)->delete());
    }

    public function legalList(Request $request)
    {
        return BaseModel::getLegalsList($request);
    }
}
