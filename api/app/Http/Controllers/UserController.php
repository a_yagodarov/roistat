<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Requests\UserBlockRequest;

class UserController extends Controller
{
    public function index(Request $request)
    {
		return User::search($request);
    }

	public function get($id)
	{
		$user = User::find($id);
		$user->phones;
		$user['permissions_list'] = implode(",", $user->getAllPermissions()->pluck('name')->toArray());
        $user->role = $user->getRoleNames()[0];
		return response()->json($user);
	}

    public function store(Request $request)
    {
	    return (new User())->store($request);
    }

    public function block(UserBlockRequest $request, User $user, $id)
    {
        $user = User::find($id);
        if ($result = $user->update($request->only('blocked', 'id')))
        {
            $user->fill($request->only('blocked', 'id'));
            return response()->json($user->save(), 200);
        }
    }

    public function update(Request $request)
    {
        $user = User::find($request->get('id'));
	    return ($user->storeUpdate($request));
    }

    public function delete($id)
    {
    	if (Auth::user()->id != $id) {
			return response()->json(User::find($id)->delete());
		}
    }

    public function legalList(Request $request)
    {
        return User::getLegalsList($request);
    }

    public function toggleStatus($id) {
		$user = User::find($id);
		$user->blocked = !$user->blocked;
		return response()->json($user->save());
	}
}
