<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Contact as BaseModel;
use App\Models\Contact;
use App\Models\Zadarma;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class ContactController extends Controller
{
	public function index(Request $request)
	{
		return BaseModel::search($request);
	}

	public function addNote(Request $request) {
		return response()->json(Contact::addNote($request));
	}

	public function get($id)
	{
		$user = BaseModel::find($id);
		$user->notes;
		foreach ($user->notes as $note) {
			$note->user;
		}
		return response()->json($user);
	}

	public function getAudio($id) {
		return response()->json((new Zadarma())->getAudio($id));
	}

	public function store(Request $request)
	{
		return (new BaseModel())->store($request);
	}

	public function update(Request $request)
	{
		$user = BaseModel::find($request->get('id'));
		return ($user->store($request));
	}

	public function delete($id)
	{
		return response()->json(BaseModel::find($id)->delete());
	}

	public function legalList(Request $request)
	{
		return BaseModel::getLegalsList($request);
	}
}
