<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name')->nullable();
			$table->integer('user_id')->unsigned()->nullable();
			$table->integer('position_id')->unsigned()->nullable();
			$table->string('phone')->nullable();
			$table->string('email')->nullable();
            $table->string('town')->nullable();
			$table->string('client_type')->nullable();
            $table->string('owner_type')->nullable();
            $table->integer('price')->nullable();
            $table->integer('month_price')->nullable();
            $table->string('address', 1000)->nullable();
			$table->integer('sms')->nullable();
			$table->integer('mailchump')->nullable();
			$table->integer('tech_type')->nullable();
			$table->integer('position')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
