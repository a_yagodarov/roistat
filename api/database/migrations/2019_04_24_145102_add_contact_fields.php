<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContactFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function (Blueprint $table) {
			$table->dateTimeTz('first_visit_date')->nullable();
			$table->dateTimeTz('first_order_date')->nullable();
			$table->dateTimeTz('last_order_date')->nullable();
			$table->dateTimeTz('birth_date')->nullable();
			$table->integer('order_count')->nullable();
			$table->integer('revenue')->nullable();
			$table->integer('profit')->nullable();
			$table->string('company')->nullable();
			$table->string('comment')->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('contacts', function (Blueprint $table) {
			$table->dropColumn('first_visit_date');
			$table->dropColumn('first_order_date');
			$table->dropColumn('last_order_date');
			$table->dropColumn('birth_date');
			$table->dropColumn('order_count');
			$table->dropColumn('revenue');
			$table->dropColumn('profit');
			$table->dropColumn('company');
			$table->dropColumn('comment');
		});
    }
}
