<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactTypeListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_type_list', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('positions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

		Schema::table('contacts', function (Blueprint $table) {
			$table->integer('client_type')->nullable()->unsigned()->change();
			$table->foreign('client_type')->references('id')->on('contact_type_list');
			$table->integer('position_id')->nullable()->unsigned()->change();
			$table->foreign('position_id')->references('id')->on('positions');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('contacts', function (Blueprint $table) {
			$table->dropForeign('contacts_client_type_foreign');
			$table->dropForeign('contacts_position_id_foreign');
//			$table->dropColumn('client_type');
//			$table->dropColumn('position_id');
		});

		Schema::dropIfExists('contact_type_list');

		Schema::dropIfExists('positions');

	}
}
