<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.12.2017
 * Time: 22:10
 */
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RPSeeder extends Seeder
{
    public function run(){
        app()['cache']->forget('spatie.permission.cache');

        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('roles')->truncate();
        DB::table('permissions')->truncate();
        DB::table('role_has_permissions')->truncate();

        $admin = Role::create(['name' => 'admin', 'title' => 'Администратор']);
        $logistician = Role::create(['name' => 'logistician', 'title' => 'Логист']);
        $agent = Role::create(['name' => 'agent', 'title' => 'Агент']);

        $manage_users = Permission::create(['name' => 'manage_users']);
        $change_password = Permission::create(['name' => '$change_password']);
        $block_user = Permission::create(['name' => 'block_user']);
        $delete_user = Permission::create(['name' => 'delete_user']);

        $manage_contacts = Permission::create(['name' => 'manage_contacts']);
        $manage_contacts_manager = Permission::create(['name' => 'manage_contact_manager']);
        $delete_contacts = Permission::create(['name' => 'delete_contacts']);

		$manage_calls = Permission::create(['name' => 'manage_calls']);

        $manage_users->assignRole($admin);
        $manage_users->assignRole($logistician);
        $manage_users->assignRole($agent);

		$block_user->assignRole($admin);
		$delete_user->assignRole($admin);
		$change_password->assignRole($admin);

        $manage_contacts->assignRole($admin);
        $manage_contacts->assignRole($logistician);
        $manage_contacts->assignRole($agent);

		$manage_contacts_manager->assignRole($admin);
		$delete_contacts->assignRole($admin);
		$manage_calls->assignRole($admin);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
