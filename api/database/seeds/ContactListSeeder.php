<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContactListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS=0');
		DB::table('contact_type_list')->truncate();
		DB::table('positions')->truncate();

		$contact_types = [
			'Поставщик техники',
			'Потенциальный клиент',
			'Клиент',
			'Не целевой',
			'Большой клиент',
		];

		$positions = [
			'Менеджер',
			'Директор'
		];

		foreach ($contact_types  as $item) {
			$model = new \App\Models\ContactType([
				'name' => $item
			]);
			$model->save();
		}

		foreach ($positions  as $item) {
			$model = new \App\Models\Position([
				'name' => $item
			]);
			$model->save();
		}

		DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
