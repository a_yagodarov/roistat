<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('authenticate', 'Auth\LoginController@authenticate');
Route::group([
		'middleware' => ['jwt.auth', 'user']
	],
	function () {
	Route::get('roistat', function () {
		return response()->json((new \App\Models\Zadarma())->fillCalls(), 200);
	});
	Route::get('zadarma', function () {
		return response()->json((new \App\Models\Roistat())->fillContacts(), 200);
	});
	Route::get('authenticate', 'Auth\LoginController@getUser');
    Route::get('verify', 'Auth\LoginController@verify');

    Route::get('users', 'UserController@index')->middleware(['jwt.auth', 'permission:manage_users']);
    Route::post('user', 'UserController@store')->middleware(['jwt.auth', 'permission:manage_users']);
    Route::post('user/status/{id}', 'UserController@toggleStatus')->middleware(['jwt.auth', 'permission:manage_users']);
    Route::get('user/{id}', 'UserController@get')->middleware(['jwt.auth', 'permission:manage_users']);
    Route::put('user', 'UserController@update')->middleware(['jwt.auth', 'permission:manage_users']);
    Route::put('user/block/{id}', 'UserController@block')->middleware(['jwt.auth', 'permission:block_user']);
    Route::delete('user/{id}', 'UserController@delete')->middleware(['jwt.auth', 'permission:delete_user']);

	Route::get('contacts', 'ContactController@index')->middleware(['jwt.auth', 'permission:manage_contacts']);
	Route::post('contact', 'ContactController@store')->middleware(['jwt.auth', 'permission:manage_contacts']);
	Route::post('note', 'ContactController@addNote')->middleware(['jwt.auth', 'permission:manage_contacts']);
	Route::get('contact/{id}', 'ContactController@get')->middleware(['jwt.auth', 'permission:manage_contacts']);
	Route::put('contact', 'ContactController@update')->middleware(['jwt.auth', 'permission:manage_contacts']);
	Route::delete('contact/{id}', 'ContactController@delete')->middleware(['jwt.auth', 'permission:manage_contacts']);

	Route::get('calls', 'CallController@index')->middleware(['jwt.auth', 'permission:manage_calls']);
	Route::get('audio/{id}', 'ContactController@getAudio')->middleware(['jwt.auth', 'permission:manage_calls']);
//	Route::post('contact', 'ContactController@store')->middleware(['jwt.auth', 'permission:manage_contacts']);
//	Route::post('note', 'ContactController@addNote')->middleware(['jwt.auth', 'permission:manage_contacts']);
//	Route::get('contact/{id}', 'ContactController@get')->middleware(['jwt.auth', 'permission:manage_contacts']);
//	Route::put('contact', 'ContactController@update')->middleware(['jwt.auth', 'permission:manage_contacts']);
//	Route::delete('contact/{id}', 'ContactController@delete')->middleware(['jwt.auth', 'permission:manage_contacts']);
});
