import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class ParamsService {
	name = 'param';
	constructor(private http: HttpClient) { }

	getAll(params= {}) {
		return this.http.get<any[]>('/api/'+this.name+'s', params);
	}
}
