import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {AuthService} from "../../../../core/auth/_services";
import {User} from "../../../../core/auth";
import {SubheaderService} from "../../../../core/_base/layout";
import {AppState} from "../../../../core/reducers";

declare var $;
declare var moment : any;
@Component({
  selector: 'kt-calls-list',
  templateUrl: './calls-list.component.html',
  styleUrls: ['./calls-list.component.scss']
})
export class CallsListComponent implements OnInit {
	datatable : any;
	user : User = new User();
	constructor(
		private subheader: SubheaderService,
		public auth: AuthService,
		public router : Router,
		private store: Store<AppState>,
		private http: HttpClient) { }

	ngOnInit() {
		this.initDatatable();
		this.subheader.setTitle("Звонки");
		this.subheader.setBreadcrumbs([
			{
				title : 'Список звонков',
				url : '/users'
			}
		]);

	}

	initDatatable() {
		const token = this.auth.getUserToken();
		this.datatable = $('#kt_datatable_calls').KTDatatable({
			// datasource definition
			data: {
				type: 'remote',
				source: {
					read: {
						method: 'GET',
						url: 'api/calls',
						headers: {
							'Authorization': token,
						},
						map: function(raw) {
							// sample data mapping
							var dataSet = raw;
							console.log(raw);
							if (typeof raw.models !== 'undefined') {
								dataSet = raw.models;
							}
							return dataSet;
						},
					},
				},
				pageSize: 10,
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true,
			},

			// layout definition
			layout: {
				scroll: false,
				footer: false,
			},

			// column sorting
			sortable: true,

			pagination: true,

			search: {
				input: $('#generalSearch'),
			},

			// columns definition
			columns: this.getColumnsByRole(),

		});
		this.datatable.on('kt-datatable--on-layout-updated', () => {
			console.log("LAYOUT UPDATE", this);
			setTimeout(() => {
				$('.contact').click((e) => {
					var id = $(e.currentTarget).attr('data-id');
					this.goToContact(id);
				});
			});
			// $(this.elRef.nativeElement).find('.m-datatable__row').click((e) => {
			//     var id = $(e.currentTarget).find('.m-datatable__cell--check').find('input').val();
			// });
		});
        //
		// this.datatable.on('kt-datatable--on-ajax-fail', ($param) => {
		// 	this.router.navigate(['/auth/login']);
		// });
	}

	goToContact(id) {
		this.router.navigate(['/contacts/edit',], { queryParams: { id: id } });
	}

	delete(id) {
		this.http.delete('/api/call/' + id).subscribe(() => {
			this.datatable.reload();
		})
	}

	getColumnsByRole() {
		var config = [];
		if (this.user.hasPermissionTo('manage_users')) {
			config = [{
				field: 'id',
				title: '#',
				sortable: 'asc',
				width: 40,
				type: 'number',
				selector: false,
				textAlign: 'center',
				autoHide: !1,
			}, {
				field: '',
				title: 'Контакт',
				autoHide: !1,
				template: function (row, index, datatable) {
					if (row['name']) {
						return '<button data-id='+row['c_id']+' type="button" class="btn btn-link">'+row['name']+'</button>';
					} else {
						return '';
					}
				}
			}, {
				field: 'number',
				title: 'Номер',
				autoHide: !1
			}, {
				field: 'callstart',
				title: 'Дата и время звонка',
				autoHide: !1,
				that:this,
				template: function (row, index, datatable) {
					return moment.utc(row['callstart']).local().format('DD.MM.YYYY HH:mm');
				}
			}];
		}
		return config;
	}

	getTrueDate(date) {
		return moment.utc(date).local().format('DD.MM.YYYY HH:mm');
	}
}
