import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CallsListComponent } from './calls-list/calls-list.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
	{ path: '', component : CallsListComponent }
];

@NgModule({
	imports: [
		ReactiveFormsModule,
		FormsModule,
		RouterModule.forChild(routes),
	],
	exports: [RouterModule],
	providers: [
	]
})
export class CallsRouteModule {
}

@NgModule({
  declarations: [CallsListComponent],
  imports: [
    CommonModule,
  	CallsRouteModule
  ]
})
export class CallsModule { }
