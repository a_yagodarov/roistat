import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../../../../core/auth";
import {SubheaderService} from "../../../../core/_base/layout";

@Component({
  selector: 'kt-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {
	config = {
		orientation : 'horizontal'
	};
	model : any = {};
	userForm : FormGroup;
	user = new User();
	roles : any = {};
	errors : any = [];
	constructor(
		private userModel: User,
		private router: Router,
		private http: HttpClient,
		private Fb:FormBuilder,
		private cdr: ChangeDetectorRef,
		private subheader: SubheaderService) {

	}


	ngOnInit() {
		this.setBreadcrumbs();
		this.createForm();

	}

	setBreadcrumbs() {
		this.subheader.setTitle("Пользователи");
		this.subheader.setBreadcrumbs([{
			title : 'Список пользователей',
			page : '/users'
		}, {
			title : 'Добавление',
			page : '/users/create'
		}
		]);
	}

	submitted = false;

	onSubmit() {
		console.log(this.userForm);
		this.http.post('api/user', this.userForm.value).subscribe((data) => {
			this.router.navigate(['/users']);
		}, (data) => {
			this.errors = data.error;
			this.cdr.detectChanges();
		});
	}

	createForm() {
		this.userForm = this.Fb.group({
			username: [this.user.username, Validators.required],
			password: [this.user.password, Validators.required],
			email: [this.user.email, Validators.email],
			first_name: [this.user.first_name, Validators.required],
			surname: [this.user.surname, Validators.required],
			patronymic: [this.user.patronymic, Validators.required],
			phones: this.Fb.array([
				this.createPhone()
			]),
			role: [this.user.role],
		});
	}

	addPhone():void {
		let items = this.userForm.get('phones') as FormArray;
		items.push(this.createPhone());
	}

	removeItem(id):void {
		let items = this.userForm.get('phones') as FormArray;
		if (items.length > 1)
			items.removeAt(id);
	}

	createPhone() {
		return this.Fb.group({
			number : ''
		});
	}

	formConfig() {
		return [
				{
					field: 'username',
					label: 'Имя пользователя',
					type: 'text',
					permission: 'manage_users'
				},
				{
					field: 'password',
					label: 'Пароль',
					type: 'password',
					permission: 'change_password'
				},
				{
					field: 'surname',
					label: 'Фамилия',
					type: 'text',
					permission: 'manage_users'
				},
				{
					field: 'first_name',
					label: 'Имя',
					type: 'text',
					permission: 'manage_users'
				},
				{
					field: 'patronymic',
					label: 'Отчество',
					type: 'text',
					permission: 'manage_users'
				},
				{
					field: 'email',
					label: 'Почта',
					type: 'email',
					permission: 'manage_users'
				},
				{
					field: 'role',
					label: 'Роль',
					type: 'select',
					permission: 'manage_users',
					options: this.userModel.roleList
				},
			]
	}

	isFieldVisible(permission) {
		console.log(this.userModel.hasPermissionTo(permission));
		return this.userModel.hasPermissionTo(permission);
	}

	trackByFn(index: any, item: any) {
		return index;
	}


	getErrorMessage(field) {
		if (this.errors[field]) {
			return this.errors[field][0];
		} else {
			return '';
		}
	}

	hasError(field)
	{
		let classList = {
			'is-invalid' : this.errors[field],
			'form-control' : true,
		};
		return classList;
	}
}
