import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../../../../core/auth";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {SubheaderService} from "../../../../core/_base/layout";

@Component({
  selector: 'kt-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {
	config = {
		orientation : 'horizontal'
	};
	model : any = {};
	userForm : FormGroup;
	user = new User();
	roles : any = {};
	errors : any = [];
	constructor(
		private subheader: SubheaderService,
		private userModel: User,
		private router: Router,
		private http: HttpClient,
		private Fb:FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute) {

	}

	ngOnInit() {
		this.createForm();
		this.initFormValues();
		this.subheader.setTitle("Пользователи");
		this.subheader.setBreadcrumbs([{
			title : 'Список пользователей',
			page : '/users'
		}, {
			title : 'Редактирование',
			page : '/users/edit',
			queryparams : {
				id : this.userForm.controls['id'].value
			}
		}
		]);
	}

	submitted = false;

	onSubmit() {
		console.log(this.userForm);
		this.http.put('api/user', this.userForm.value).subscribe((data) => {
			this.router.navigate(['/users']);
		}, (data) => {
			this.errors = data.error;
			this.cdr.detectChanges();
		});
	}

	initFormValues() {
		this.route.data
			.subscribe((data) => {
				console.log(data);
				this.userForm.patchValue(data['data'], {
					emitEvent : false,
					onlySelf : true
				});
				this.setPhonesValue(data['data']);
			});
	}

	createForm() {
		this.userForm = this.Fb.group({
			id: [this.user.id, Validators.required],
			username: [this.user.username, Validators.required],
			password: [this.user.password, Validators.required],
			email: [this.user.email, Validators.email],
			first_name: [this.user.first_name, Validators.required],
			surname: [this.user.surname, Validators.required],
			patronymic: [this.user.patronymic, Validators.required],
			role: [this.user.role],
			// phones: this.Fb.array([
			// 	this.createPhone()
			// ]),
		});
	}

	addPhone():void {
		let items = this.userForm.get('phones') as FormArray;
		items.push(this.createPhone());
	}

	removeItem(id):void {
		let items = this.userForm.get('phones') as FormArray;
		if (items.length > 1)
			items.removeAt(id);
	}

	createPhone() {
		return this.Fb.group({
			number : ''
		});
	}

	setPhonesValue(data)
	{
		console.log(this.userForm);
		if (data.phones.length)
		{
			let items = [];
			let phones;

			// phones = this.userForm.get('phones') as FormArray;
			data.phones.forEach((phone) => {
				let newFormGroup = this.Fb.group({
					number: phone.number,
				});

				items.push(newFormGroup);
			});
			console.log(items);
			this.userForm.addControl('phones',this.Fb.array(items));
			console.log(this.userForm);
		}
		else
		{
			let items = [];
			let newFormGroup = this.Fb.group({
				number: '',
			});
			items.push(newFormGroup);
			this.userForm.addControl('phones',this.Fb.array(items));
			// this.userForm.addControl('phones',this.Fb.array([this.addPhone()]));
		}
	}

	formConfig() {
		return [
			{
				field: 'username',
				label: 'Имя пользователя',
				type: 'text',
				permission: 'manage_users'
			},
			{
				field: 'password',
				label: 'Пароль',
				type: 'password',
				permission: 'change_password'
			},
			{
				field: 'surname',
				label: 'Фамилия',
				type: 'text',
				permission: 'manage_users'
			},
			{
				field: 'first_name',
				label: 'Имя',
				type: 'text',
				permission: 'manage_users'
			},
			{
				field: 'patronymic',
				label: 'Отчество',
				type: 'text',
				permission: 'manage_users'
			},
			{
				field: 'email',
				label: 'Почта',
				type: 'email',
				permission: 'manage_users'
			},
			{
				field: 'role',
				label: 'Роль',
				type: 'select',
				permission: 'manage_users',
				options: this.userModel.roleList
			},
		]
	}

	isFieldVisible(permission) {
		console.log(this.userModel.hasPermissionTo(permission));
		return this.userModel.hasPermissionTo(permission);
	}

	trackByFn(index: any, item: any) {
		return index;
	}

	getErrorMessage(field) {
		if (this.errors[field]) {
			return this.errors[field][0];
		} else {
			return '';
		}
	}

	hasError(field)
	{
		let classList = {
			'is-invalid' : this.errors[field],
			'form-control' : true,
		};
		return classList;
	}
}
