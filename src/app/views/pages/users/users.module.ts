import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from './users-list/users-list.component';
import { UserCreateComponent } from './user-create/user-create.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ComponentFixture} from "@angular/core/testing";
import {async} from "q";
import {UserEditResolver} from "../../../core/user/resolvers/user";
import {User} from "../../../core/auth";

const routes: Routes = [
	{ path: '', component : UsersListComponent },
	{ path: 'create', component : UserCreateComponent },
	{ path: 'edit', component : UserEditComponent , resolve : { data : UserEditResolver }},
];

@NgModule({
	imports: [
		ReactiveFormsModule,
		FormsModule,
		RouterModule.forChild(routes),
	],
	exports: [RouterModule],
	providers: [
		UserEditResolver,
		User
	]
})
export class UsersRouteModule {
}
@NgModule({
  declarations: [UsersListComponent, UserCreateComponent, UserEditComponent],
  imports: [
    CommonModule,ReactiveFormsModule,
	  FormsModule,
  	UsersRouteModule
  ]
})
export class UsersModule { }
