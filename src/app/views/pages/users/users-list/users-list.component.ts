import {AfterViewInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {SubheaderService} from '../../../../core/_base/layout';
import {AuthService} from '../../../../core/auth/_services';
import {Router} from '@angular/router';
import {Logout, User} from "../../../../core/auth";
import {Store} from "@ngrx/store";
import {AppState} from "../../../../core/reducers";
import {HttpClient} from "@angular/common/http";

declare var $;
@Component({
  selector: 'kt-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
	datatable : any;
	user : User = new User();
  constructor(
  	private subheader: SubheaderService,
	public auth: AuthService,
	public router : Router,
	private store: Store<AppState>,
	private http: HttpClient) { }

  ngOnInit() {
  	this.initDatatable();
  	this.subheader.setTitle("Пользователи");
	  this.subheader.setBreadcrumbs([
		  {
			  title : 'Список пользователей',
			  url : '/users'
		  }
	  ]);

  }

	redirectCreateUser() {
		this.router.navigate(['/users/create']);
	}

  initDatatable() {
  	const token = this.auth.getUserToken();
	  this.datatable = $('#kt_datatable_users').KTDatatable({
		  // datasource definition
		  data: {
			  type: 'remote',
			  source: {
				  read: {
					  method: 'GET',
					  url: 'api/users',
					  headers: {
						  'Authorization': token,
					  },
					  map: function(raw) {
						  // sample data mapping
						  var dataSet = raw;
						  console.log(raw);
						  if (typeof raw.models !== 'undefined') {
							  dataSet = raw.models;
						  }
						  return dataSet;
					  },
				  },
			  },
			  pageSize: 10,
			  serverPaging: true,
			  serverFiltering: true,
			  serverSorting: true,
		  },

		  // layout definition
		  layout: {
			  scroll: false,
			  footer: false,
		  },

		  // column sorting
		  sortable: true,

		  pagination: true,

		  search: {
			  input: $('#generalSearch'),
		  },

		  // columns definition
		  columns: this.getColumnsByRole(),

	  });
	  this.datatable.on('kt-datatable--on-layout-updated', () => {
		  console.log("LAYOUT UPDATE", this);
		  setTimeout(() => {
			  $('.editUser').click((e) => {
				  var id = $(e.currentTarget).attr('data-id');
				  this.edit(id);
			  });
			  $('.deleteUser').click((e) => {
				  var id = $(e.currentTarget).attr('data-id');
				  if (confirm('Вы уверены что хотите удалть?')) {
					  this.delete(id);
				  }
			  });
			  $('.toggleStatus').click((e) => {
				  var id = $(e.currentTarget).attr('data-id');
				  this.toggleStatus(id);
			  });
		  });
		  // $(this.elRef.nativeElement).find('.m-datatable__row').click((e) => {
		  //     var id = $(e.currentTarget).find('.m-datatable__cell--check').find('input').val();
		  // });
	  });

	  this.datatable.on('kt-datatable--on-ajax-fail', ($param) => {
		  this.router.navigate(['/auth/login']);
	  });
  }

  toggleStatus(id) {
	  this.http.post('/api/user/status/' + id, {}).subscribe(() => {
		  this.datatable.reload();
	  })
  }

  edit(id) {
  	this.router.navigate(['/users/edit',], { queryParams: { id: id } });
  }

  delete(id) {
  	this.http.delete('/api/user/' + id).subscribe(() => {
  		this.datatable.reload();
	})
  }

  getColumnsByRole() {
  	var config = [];
  	if (this.user.hasPermissionTo('manage_users')) {
  		config = [{
			field: 'id',
			title: '#',
			sortable: 'asc',
			width: 40,
			type: 'number',
			selector: false,
			textAlign: 'center',
			autoHide: !1,
		}, {
			field: 'name',
			title: 'Name',
			autoHide: !1,
			template: function(row, index, datatable) {
				return row.first_name + ' ' + row.surname;
			},
		}, {
			field: 'email',
			title: 'Email',
			autoHide: !1,
		}, {
			field: 'username',
			title: 'Username',
			autoHide: !1,
		}, {
			field: 'roles',
			title: 'Роль',
			width: 90,
			template: function(row) {
				let roleList = [
					{value:'admin', 'label': 'Администратор'},
					{value:'logistician', 'label': 'Логист'},
					{value:'agent', 'label': 'Агент'}];
				let foundValue = roleList.find(obj=>obj.value === row['role']);
				return foundValue.label;
			}
		}];
	}
	if (this.user.hasPermissionTo('block_user')) {
		config.push({
			field: 'blocked',
			title: 'Статус',
			width: 140,
			template: function (row, index, datatable) {
				if (row['blocked'] == 1) {
					var html = ' <button data-id='+row['id']+'type="button" class="toggleStatus btn btn-danger btn-sm btn-pill">Заблокирован</button>';
				} else {
					var html = ' <button data-id='+row['id']+'type="button" class="toggleStatus btn btn-success btn-sm btn-pill">Активен</button>';
				}
				return html;
			}
		})
	}
	config.push({
	  field: 'phones',
	  title: 'Телефоны',
	  template: function (row, index, datatable) {
		  var html = '';
		  if (row['phones']) {
			  row['phones'].forEach((item) => {
				  html += '<p>'+item['number']+'</p>'
			  });
		  }
		  return html;
	  }
	},{
		field: 'Actions',
		title: 'Actions',
		sortable: false,
		overflow: 'visible',
		textAlign: 'center',
		autoHide: !1,
		that: this,
		template: function(row, index, datatable) {
			let visible = this.that.user.hasPermissionTo('delete_user');
			var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
			let result =  '<button href="javascript:;" data-id='+row['id']+' class="editUser btn btn-hover-brand btn-icon btn-pill" title="Edit details">\
                        <i class="la la-edit"></i>\
                    </button>';
			if (visible) {
				result +=
					'<button href="javascript:;" data-id='+row['id']+' class="deleteUser btn btn-hover-danger btn-icon btn-pill" title="Delete">\
                        <i class="la la-trash"></i>\
                    </button>'
			}
			return result;
		},
	});
  	return config;
  }
}
