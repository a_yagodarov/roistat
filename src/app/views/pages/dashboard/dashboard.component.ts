// Angular
import { Component, OnInit } from '@angular/core';
// Lodash
import { shuffle } from 'lodash';
// Services
import { LayoutConfigService } from '../../../core/_base/layout';
// Widgets model
import { SparklineChartOptions } from '../../../core/_base/metronic';
import { Widget4Data } from '../../partials/content/widgets/widget4/widget4.component';
import {HttpClient} from "@angular/common/http";

@Component({
	selector: 'kt-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
	constructor(
		private http:HttpClient
	) {

	}
	ngOnInit() {

	}

	fillCallsAndContactsZadarma() {
		this.http.request('GET', 'api/zadarma').subscribe((data) => {
			console.log(data);
		}, error => {
			console.log(error);
		});
	}

	fillContactsRoistat() {
		this.http.request('GET', 'api/roistat').subscribe((data) => {
			console.log(data);
		}, error => {
			console.log(error);
		});
	}
}
