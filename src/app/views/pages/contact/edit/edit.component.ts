import {AfterViewInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../../../../core/auth";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {SubheaderService} from "../../../../core/_base/layout";
declare var $ : any;
declare var moment : any;
@Component({
  selector: 'kt-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit, AfterViewInit {
	title = "";
	config = {
		orientation : 'horizontal'
	};
	model : any = {};
	notes : any;
	userForm : FormGroup;
	noteForm = {
		comment : '',
		contact_id : '',
		note_type : 1
	};
	users : any = [];
	roles : any = {};
	errors : any = [];
	constructor(
		private subheader: SubheaderService,
		private userModel: User,
		private router: Router,
		private http: HttpClient,
		private Fb:FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute) {
		window['a'] = this;
	}

	ngOnInit() {
		this.getUserList();
		this.createForm();
		this.initFormValues();
	}

	ngAfterViewInit() {
		this.setScrollBot();
		this.initBsTagsInput();
	}

	setSubheaderEditTitle() {
		this.subheader.setTitle("Контакты");
		this.subheader.appendBreadcrumbs([
			{
				title : 'Список',
				url : '/contacts'
			}, {
				title : 'Редактирование',
				page : '/contacts/edit',
				queryparams : {
					id : this.userForm.controls['id'].value
				}
			}
		]);
	};

	setSubheaderCreateTitle() {
		this.subheader.setTitle("Контакты");
		this.subheader.appendBreadcrumbs([
			{
				title : 'Список контактов',
				url : '/contacts'
			}, {
				title : 'Добавление',
				page : '/contacts/edit'
			}
		]);
	};

	submitted = false;

	setAudio(id) {
		$('#sound').stop();
		this.getAudio(id).subscribe((data) => {
			let link = data['link'];
			$('#sound').attr('src', link);
			$('#sound a').attr("src", link);
			// $('#sound').attr('src', "https://sample-videos.com/audio/mp3/crowd-cheering.mp3");
			// $('#sound a').attr("src", "https://sample-videos.com/audio/mp3/crowd-cheering.mp3");
			$('#sound')[0].play();
		})
	}

	onSubmit() {
		this.userForm.controls.tags.setValue($('#tags').val());
		if (this.userForm.controls.id.value) {
			this.http.put('api/contact', this.userForm.value).subscribe((data) => {
				this.router.navigate(['/contacts']);
			}, (data) => {
				this.errors = data.error;
				this.cdr.detectChanges();
			});
		} else {
			this.http.post('api/contact', this.userForm.value).subscribe((data) => {
				this.router.navigate(['/contacts']);
			}, (data) => {
				this.errors = data.error;
				this.cdr.detectChanges();
			});
		}
	}

	getAudio(id) {
		return this.http.request('GET', '/api/audio/'+id);
	}

	addNote() {
		this.http.post('/api/note', this.noteForm).subscribe(() => {
			this.noteForm.comment = '';
		});
	}

	getUserList() {
		this.http.get('/api/users').subscribe((data) => {
			console.log(data);
			data['models'].forEach((item) => {
				this.users.push({
					'id' : item['id'],
					'name' : item['first_name'] + ' ' + item['surname']
				})
			});
			this.cdr.detectChanges();
		})
	}

	getUserFullName(note) {
		if (note.user != null) {
			return (note.user.first_name + ' ' + note.user.surname);
		} else {
			return "Система";
		}
	}
	setScrollBot()
	{
		let div = $('.scroll');
		div.scrollTop(div.prop("scrollHeight"));
	}

	initFormValues() {
		if (this.route.data) {
			this.title = "Редактирование контакта";
			this.route.data
				.subscribe((data) => {
					if (data['data']) {
						this.notes = data['data']['notes'].concat(data['data']['calls']);
						this.notes.sort((a,b) => { return moment(a['created_at']).format("X") - moment(b['created_at']).format("X")});
						this.noteForm.contact_id = data['data']['id'];
						this.userForm.patchValue(data['data'], {
							emitEvent : false,
							onlySelf : true
						});
						this.setSubheaderEditTitle();
					} else {
						this.setSubheaderCreateTitle();
					}
				});
		}
		this.title = "Добавление контакта";
	}

	initBsTagsInput() {
		$('#tags').tagsinput({
			allowDuplicates: false
		});
		setTimeout(() => {
				this.route.data
					.subscribe((data) => {
						if (data['data']) {
							let tags = data['data']['tags'];
							$('#tags').tagsinput('add', tags);
						}
					})});
	}

	formConfig() {
		return [
			{
				field: 'company',
				label: 'Компания клиента',
				type: 'text',
				permission: 'manage_contacts',
			},
			{
				field: 'name',
				label: 'Имя клиента',
				type: 'text',
				permission: 'manage_contacts',
			},
			{
				field: 'number',
				label: 'Раб.телефон',
				type: 'text',
				permission: 'manage_contacts',
			},
			{
				field: 'email',
				label: 'Email раб.',
				type: 'text',
				permission: 'manage_contacts',
			},
			{
				field: 'town',
				label: 'Город',
				type: 'text',
				permission: 'manage_contacts',
			},
			{
				field: 'client_type',
				label: 'Тип клиента',
				type: 'select',
				options : function () {
					return (JSON.parse(localStorage.getItem('options'))['contact_types_list']);
				},
				permission: 'manage_contacts',
			},
			{
				field: 'position_id',
				label: 'Тип собственника',
				type: 'select',
				options : function () {
					return (JSON.parse(localStorage.getItem('options'))['positions']);
				},
				permission: 'manage_contacts',
			},
			{
				field: 'revenue',
				label: 'Выручка по заказам',
				type: 'number',
				permission: 'manage_contacts',
			},
			{
				field: 'order_count',
				label: 'Количество заказов',
				type: 'number',
				permission: 'manage_contacts',
			},
			{
				field: 'profit',
				label: 'Прибыль по заказам',
				type: 'number',
				permission: 'manage_contacts',
			},
			{
				field: 'comment',
				label: 'Комментарий',
				type: 'text',
				permission: 'manage_contacts',
			},
			{
				field: 'tags',
				label: 'Теги',
				type: 'tagsinput',
				permission: 'manage_contacts',
			},
			{
				field: 'user_id',
				label: 'Ответственный',
				type: 'select',
				options : () => {
					return this.users;
				},
				permission: 'manage_contact_manager',
			},
		]
	}

	createForm() {
		this.userForm = this.Fb.group({
			id: [this.model.id, Validators.required],
			name: [this.model.name, Validators.required],
			number: [this.model.number, Validators.required],
			email: [this.model.email, Validators.required],
			town: [this.model.town, Validators.required],
			client_type: [this.model.client_type, Validators.required],
			position_id: [this.model.position_id, Validators.required],
			owner_type: [this.model.owner_type, Validators.required],
			price: [this.model.price, Validators.required],
			tags: [this.model.tags, Validators.required],
			user_id: [this.model.user_id, Validators.required],
			company: [this.model.company, Validators.required],
			comment: [this.model.comment, Validators.required],
			profit: [this.model.profit, Validators.required],
			order_count: [this.model.order_count, Validators.required],
			revenue: [this.model.revenue, Validators.required],
			// phones: this.Fb.array([
			// 	this.createPhone()
			// ]),
		});
	}
	isFieldVisible(permission) {
		console.log(this.userModel.hasPermissionTo(permission));
		return this.userModel.hasPermissionTo(permission);
	}

	trackByFn(index: any, item: any) {
		return index;
	}

	getErrorMessage(field) {
		if (this.errors[field]) {
			return this.errors[field][0];
		} else {
			return '';
		}
	}

	hasError(field)
	{
		let classList = {
			'is-invalid' : this.errors[field],
			'form-control' : true,
		};
		return classList;
	}


	getTrueDate(date) {
		return moment.utc(date).local().format('DD.MM.YYYY HH:mm');
	}
}
