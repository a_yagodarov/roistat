import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {SubheaderService} from "../../../../core/_base/layout";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {AuthService} from "../../../../core/auth/_services";
import {AppState} from "../../../../core/reducers";
import {User} from "../../../../core/auth";

declare var $;
@Component({
  selector: 'kt-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
	datatable : any;
	search : any = {asd:'asd'};
	constructor(
		public subheader: SubheaderService,
		public auth: AuthService,
		private user: User,
		public router : Router,
		private store: Store<AppState>,
		private http: HttpClient) { }

	ngOnInit() {
		this.subheader.setTitle('Контакты')
		this.subheader.setBreadcrumbs([
			{
				title : 'Список контактов',
				url : '/contacts'
			}
		]);
		this.initDatatable();
		this.initBsTagsInput();
	}

	initBsTagsInput() {
		$('#tagsinput').tagsinput({
			allowDuplicates: false
		});
		$('#tagsinput').on('itemAdded', (event) => {
			this.datatable.search($('#tagsinput').val(), 'tags');
		});
		$('#tagsinput').on('itemRemoved', (event) => {
			this.datatable.search($('#tagsinput').val(), 'tags');
		});
	}

	initDatatable() {
		const token = this.auth.getUserToken();
		this.datatable = $('#kt_datatable_contacts').KTDatatable({
			// datasource definition
			data: {
				type: 'remote',
				source: {
					read: {
						params: {value: $('#tagsinput').val()},
						method: 'GET',
						url: 'api/contacts',
						headers: {
							'Authorization': token,
						},
						map: function(raw) {
							// sample data mapping
							var dataSet = raw;
							if (typeof raw.models !== 'undefined') {
								dataSet = raw.models;
							}
							return dataSet;
						},
					},
				},
				pageSize: 10,
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true,
			},

			// layout definition
			layout: {
				scroll: false,
				footer: false,
			},

			// column sorting
			sortable: true,

			pagination: true,

			search: {
				input: $('#generalSearch'),
			},

			// columns definition
			columns: [
				{
					field: 'id',
					title: '#',
					sortable: 'asc',
					width: 40,
					type: 'number',
					selector: false,
					textAlign: 'center',
					autoHide: !1,
				}, {
					field: 'company',
					title: 'Компания клиента',
					autoHide: !1,
					template: function(row, index, datatable) {
						return row.name;
					},
				}, {
					field: 'number',
					title: 'Телефон',
					autoHide: !1,
				}, {
					field: 'email',
					title: 'Эл. почта',
					autoHide: !1,
				}, {
					field: 'position_id',
					title: 'Должность',
					width: 90,
					template: function (row, index, datatable) {
						if (row['position_id']) {
							return ((JSON.parse(localStorage.getItem('options')))['positions']).find(x => x.id == row['position_id']).name;
						} else {
							return "";
						}
					}
				},{
					field: 'Actions',
					title: 'Действия',
					sortable: false,
					overflow: 'visible',
					textAlign: 'center',
					autoHide: !1,
					template: (row, index, datatable) => {
						let result = '<button href="javascript:;" data-id='+row['id']+' class="editUser btn btn-hover-brand btn-icon btn-pill" title="Edit details">\
							<i class="la la-edit"></i>\
						</button>'
						if (this.user.hasPermissionTo('delete_contacts')) {
							result += '<button href="javascript:;" data-id='+row['id']+' class="deleteUser btn btn-hover-danger btn-icon btn-pill" title="Delete">\
								<i class="la la-trash"></i>\
							</button>';
						}
						return result;
					},
				}],

		});
		this.datatable.on('kt-datatable--on-layout-updated', () => {
			setTimeout(() => {
				$('.editUser').click((e) => {
					var id = $(e.currentTarget).attr('data-id');
					this.edit(id);
				});
				$('.deleteUser').click((e) => {
					var id = $(e.currentTarget).attr('data-id');
					if (confirm('Вы уверены что хотите удалть?')) {
						this.delete(id);
					}
				});
			});
			// $(this.elRef.nativeElement).find('.m-datatable__row').click((e) => {
			//     var id = $(e.currentTarget).find('.m-datatable__cell--check').find('input').val();
			// });
		});
		console.log(this.datatable);
		this.datatable.on('kt-datatable--on-ajax-fail', ($param) => {
			console.log($param);
			// this.router.navigate(['/auth/login']);
		});
	}

	edit(id) {
		this.router.navigate(['/contacts/edit',], { queryParams: { id: id } });
	}

	delete(id) {
		this.http.delete('/api/contact/' + id).subscribe(() => {
			this.datatable.reload();
		})
	}
}
