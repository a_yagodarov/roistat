import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import {RouterModule, Routes} from "@angular/router";
import {User} from "../../../core/auth";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ContactEditResolver} from "../../../core/contact/resolvers/contact";

const routes: Routes = [
	{ path: '', component : ListComponent },
	{
		path: 'edit',
		component : EditComponent ,
		resolve : {
			data : ContactEditResolver
		}
	},
	{
		path: 'create',
		component : EditComponent
	},
];

@NgModule({
	imports: [
		ReactiveFormsModule,
		FormsModule,
		RouterModule.forChild(routes),
	],
	exports: [RouterModule],
	providers: [
		ContactEditResolver,
		User
	]
})
export class ContactRouteModule {
}

@NgModule({
  declarations: [ListComponent, EditComponent],
  imports: [
    CommonModule,
	  ReactiveFormsModule,
	  FormsModule,
	  ContactRouteModule
  ]
})
export class ContactModule { }
